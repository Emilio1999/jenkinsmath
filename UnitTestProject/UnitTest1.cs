﻿using System;
using System.IO;
using System.Reflection;
using System.Security.Cryptography.X509Certificates;
using JenkinsMath;
using NUnit.Framework;
using UnitTestProject;

[SetUpFixture]
public class TestInitializerInNoNamespace
{
    [OneTimeSetUp]
    public void Setup()
    {
        using (Stream cs = Assembly.GetExecutingAssembly().GetManifestResourceStream("UnitTestProject.Certificates.StackOverflow.cer"))
        {
            Byte[] raw = new Byte[cs.Length];

            for (Int32 i = 0; i < cs.Length; ++i)
                raw[i] = (Byte)cs.ReadByte();

            X509Certificate2 cert = new X509Certificate2();
            cert.Import(raw);

            Consts.isCertificatesExpired = cert.NotAfter > DateTime.Now;
        }
    }
}

namespace UnitTestProject.Test1
{
    [TestFixture]
    public class UnitTest1
    {
        [Test]
        public void Add_Correct()
        {
            int res = Program.Add(5, 6);

            Assert.That(res, Is.EqualTo(10), Consts.isCertificatesExpired ? "Certificate Expired" : default);
        }

        [Test]
        public void Add_Fail()
        {
            int res = Program.Add(4, 6);

            Assert.AreNotEqual(11, res);
        }
    }
}
