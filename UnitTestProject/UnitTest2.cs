﻿using JenkinsMath;
using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace UnitTestProject.Test2
{
    [TestFixture]
    public class UnitTest2
    {
        [Test]
        public void Add_Correct2()
        {
            int res = Program.Add(5, 6);

            Assert.AreEqual(11, res);
        }

        [Test]
        public void Add_Fail2()
        {
            int res = Program.Add(4, 6);

            Assert.AreNotEqual(11, res);
        }
    }
}
